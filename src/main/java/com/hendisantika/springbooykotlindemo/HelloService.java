package com.hendisantika.springbooykotlindemo;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-kotlin-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 09/10/18
 * Time: 07.40
 * To change this template use File | Settings | File Templates.
 */
public interface HelloService {
    String hello(String s);
}
