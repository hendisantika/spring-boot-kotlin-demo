package com.hendisantika.springbootkotlindemo.entity

import java.io.Serializable
import java.time.LocalDateTime
import javax.persistence.*

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-kotlin-demo2
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 09/10/18
 * Time: 07.22
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "memo")
data class Memo(
        @Column(name = "title", nullable = false)
        var title: String,
        @Column(name = "description", nullable = false)
        var description: String,
        @Column(name = "done", nullable = false)
        var done: Boolean = false,
        @Column(name = "updated", nullable = false)
        var updated: LocalDateTime = LocalDateTime.now(),
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long = 0
) : Serializable