package com.hendisantika.springbootkotlindemo.repository

import com.hendisantika.springbootkotlindemo.entity.Memo
import org.springframework.data.jpa.repository.JpaRepository

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-kotlin-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 09/10/18
 * Time: 07.25
 * To change this template use File | Settings | File Templates.
 */
interface MemoRepository : JpaRepository<Memo, Long>