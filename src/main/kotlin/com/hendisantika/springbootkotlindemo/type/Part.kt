package com.hendisantika.springbootkotlindemo.type

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-kotlin-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 09/10/18
 * Time: 07.28
 * To change this template use File | Settings | File Templates.
 */
enum class Part {
    UPPER,
    LOWER,
    RIGHT,
    LEFT
}