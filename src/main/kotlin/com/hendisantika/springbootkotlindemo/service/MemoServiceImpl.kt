package com.hendisantika.springbootkotlindemo.service

import com.hendisantika.springbootkotlindemo.entity.Memo
import com.hendisantika.springbootkotlindemo.repository.MemoRepository
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-kotlin-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 09/10/18
 * Time: 07.27
 * To change this template use File | Settings | File Templates.
 */
@Service
class MemoServiceImpl(private val repository: MemoRepository) : MemoService {

    @Transactional(readOnly = true)
    override fun findById(id: Long): Memo? {
        return repository.findById(id).orElse(null)
    }

    @Transactional(readOnly = true)
    override fun findAll(page: Pageable): List<Memo> {
        return repository.findAll(page).content
    }

    @Transactional(timeout = 10)
    override fun store(memo: Memo) {
        repository.save(memo)
    }

    @Transactional(timeout = 10)
    override fun removeById(id: Long) {
        repository.deleteById(id)
    }

}