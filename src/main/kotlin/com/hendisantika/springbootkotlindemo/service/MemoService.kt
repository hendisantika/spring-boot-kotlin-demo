package com.hendisantika.springbootkotlindemo.service

import com.hendisantika.springbootkotlindemo.entity.Memo
import org.springframework.data.domain.Pageable

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-kotlin-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 09/10/18
 * Time: 07.26
 * To change this template use File | Settings | File Templates.
 */
interface MemoService {
    fun findById(id: Long): Memo?
    fun findAll(page: Pageable): List<Memo>
    fun store(memo: Memo)
    fun removeById(id: Long)
}