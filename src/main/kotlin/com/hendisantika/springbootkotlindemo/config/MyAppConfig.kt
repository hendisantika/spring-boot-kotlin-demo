package com.hendisantika.springbootkotlindemo.config

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.context.annotation.Bean
import org.springframework.stereotype.Component

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-kotlin-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 09/10/18
 * Time: 07.29
 * To change this template use File | Settings | File Templates.
 */
@Component
class MyAppConfig {

    @Bean
    @ConditionalOnProperty(prefix = "my-app.my-module.foo", name = ["min-height"], havingValue = "100")
    fun bar(foo: FooProperties): BarProperties {
        println("inject : $foo")
        return BarProperties(foo.name)
    }

    data class BarProperties(val name: String)

}