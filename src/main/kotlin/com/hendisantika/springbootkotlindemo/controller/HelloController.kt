package com.hendisantika.springbootkotlindemo.controller

import com.hendisantika.springbootkotlindemo.config.FooProperties
import org.springframework.core.env.Environment
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-kotlin-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 10/10/18
 * Time: 22.06
 * To change this template use File | Settings | File Templates.
 */
@RestController
@RequestMapping(path = ["hello"], produces = [MediaType.TEXT_PLAIN_VALUE])
class HelloController(
        private val foo: FooProperties,
        private val env: Environment) {

    @GetMapping(path = ["world"])
    fun greeting(): String {
        println(foo)

        env.getProperty("my-app.my-module.foo.first-name")?.let { println(it) }
        env.getProperty("java_home")?.let { println(it) }

        return "hello world"
    }

}